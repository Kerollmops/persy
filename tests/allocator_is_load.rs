#[test]
#[ignore]
fn test_multithread_many_instance_insert() {
    const TEST_PERSY_PATH: &'static str = "./test_multithread_many_instance_insert.persy";
    let _ = std::fs::remove_file(TEST_PERSY_PATH);
    {
        use persy::{Config, Persy};

        Persy::create(TEST_PERSY_PATH).expect("persy create");
        let persy = Persy::open(TEST_PERSY_PATH, Config::new()).expect("persy open");

        let mut tx = persy.begin().expect("persy begin");
        persy
            .create_index::<i32, i32>(&mut tx, "tdbi", persy::ValueMode::REPLACE)
            .expect("persy create_index");
        let prepared = persy.prepare_commit(tx).expect("persy prepare_commit");
        persy.commit(prepared).expect("persy commit");

        let my_insert = |persy: &Persy, key: i32, value: i32| {
            let mut tx = persy.begin().expect("persy begin");
            persy.put::<i32, i32>(&mut tx, "tdbi", key, value).expect("persy put");
            let prepared = persy.prepare_commit(tx).expect("persy prepare_commit");
            persy.commit(prepared).expect("persy commit");
        };

        let spawn_worker = |r| {
            let own_persy = persy.clone();
            thread::spawn(move || {
                let mut stdout = std::io::stdout();
                for i in r {
                    use std::io::Write;
                    if (i % 10) == 9 {
                        write!(&mut stdout, "{} ", i).unwrap();
                        let _ = stdout.flush();
                    }
                    my_insert(&own_persy, i, i);
                }
            })
        };

        let t_1 = spawn_worker(0..1000);
        let t_2 = spawn_worker(0..1000);
        let t_3 = spawn_worker(1000..2000);
        let t_4 = spawn_worker(1000..2000);

        assert!(t_1.join().is_ok());
        assert!(t_2.join().is_ok());
        assert!(t_3.join().is_ok());
        assert!(t_4.join().is_ok());
    }

    use std::{thread, time};

    thread::sleep(time::Duration::from_secs(1));
    let _ = std::fs::remove_file(TEST_PERSY_PATH);
}
