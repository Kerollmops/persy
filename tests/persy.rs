mod helpers;

use helpers::{create_and_drop, CountDown};
use persy::{Config, Persy};
use std::fs;
use std::sync::Arc;
use std::thread;
use tempfile::{tempfile, Builder};

#[test]
fn create() {
    Persy::create_from_file(tempfile().unwrap()).unwrap();
}

#[test]
fn lock_double_open() {
    {
        Persy::create("./target/file_dd.persy").unwrap();
        let open = Persy::open("./target/file_dd.persy", Config::new());
        assert!(!open.is_err());
        let open1 = Persy::open("./target/file_dd.persy", Config::new());
        assert!(open1.is_err());
    }
    fs::remove_file("./target/file_dd.persy").unwrap();
}

#[test]
fn fail_double_create() {
    {
        let res = Persy::create("./target/file2.persy");
        assert!(!res.is_err());
    }
    let res = Persy::create("./target/file2.persy");
    fs::remove_file("./target/file2.persy").unwrap();
    assert!(res.is_err());
}

#[test]
fn create_open() {
    let file = Builder::new()
        .prefix("open")
        .suffix(".persy")
        .tempfile()
        .expect("expect temp file creation");
    Persy::create_from_file(file.reopen().unwrap()).unwrap();
    let open = Persy::open_from_file(file.reopen().unwrap(), Config::new());
    assert!(!open.is_err());
}

#[test]
fn test_rollback() {
    create_and_drop("rollback", |persy| {
        let mut tx = persy.begin().unwrap();
        persy.create_segment(&mut tx, "test").unwrap();
        let finalizer = persy.prepare_commit(tx).unwrap();
        persy.commit(finalizer).unwrap();

        let mut tx = persy.begin().unwrap();
        let rec_data: String = "something".into();
        let bytes = rec_data.into_bytes();
        let id = persy.insert_record(&mut tx, "test", &bytes).unwrap();
        persy.rollback(tx).unwrap();
        let read_after = persy.read_record("test", &id).unwrap();
        if let Some(_) = read_after {
            assert!(false);
        } else {
            assert!(true);
        }
    });
}

#[test]
fn test_rollback_precommit() {
    create_and_drop("rollback_pre", |persy| {
        let mut tx = persy.begin().unwrap();
        persy.create_segment(&mut tx, "test").unwrap();
        let finalizer = persy.prepare_commit(tx).unwrap();
        persy.commit(finalizer).unwrap();

        let mut tx = persy.begin().unwrap();
        let rec_data: String = "something".into();
        let bytes = rec_data.into_bytes();
        let id = persy.insert_record(&mut tx, "test", &bytes).unwrap();
        let finalizer = persy.prepare_commit(tx).unwrap();
        persy.rollback_prepared(finalizer).unwrap();
        let read_after = persy.read_record("test", &id).unwrap();
        if let Some(_) = read_after {
            assert!(false);
        } else {
            assert!(true);
        }
    });
}

#[test]
fn test_rollback_update() {
    create_and_drop("rollback_update", |persy| {
        let mut tx = persy.begin().unwrap();
        persy.create_segment(&mut tx, "test").unwrap();
        let rec_data: String = "something".into();
        let bytes = rec_data.into_bytes();
        let id = persy.insert_record(&mut tx, "test", &bytes).unwrap();
        let read_opt = persy.read_record_tx(&mut tx, "test", &id).unwrap();
        if let Some(read) = read_opt {
            assert_eq!(bytes, read);
        } else {
            assert!(false);
        }
        let finalizer = persy.prepare_commit(tx).unwrap();
        persy.commit(finalizer).unwrap();

        let mut tx1 = persy.begin().unwrap();
        let rec_data_1: String = "something2".into();
        let bytes_1 = rec_data_1.into_bytes();
        persy.update_record(&mut tx1, "test", &id, &bytes_1).unwrap();
        let read_after = persy.read_record_tx(&mut tx1, "test", &id).unwrap();
        if let Some(val) = read_after {
            assert_eq!(val, bytes_1);
        } else {
            assert!(false);
        }
        persy.rollback(tx1).unwrap();

        let read_after = persy.read_record("test", &id).unwrap();
        if let Some(val) = read_after {
            assert_eq!(val, bytes);
        } else {
            assert!(false);
        }
    });
}

#[test]
pub fn councurrent_create() {
    create_and_drop("rollback_create", |persy| {
        let mut tx = persy.begin().expect("error on transactoin begin");
        persy.create_segment(&mut tx, "def").expect("error on segment creation");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");

        let count = Arc::new(CountDown::new(2));

        for _ in &[1, 2] {
            let count = count.clone();
            let persy = persy.clone();
            thread::spawn(move || {
                let mut tx = persy.begin().expect("error on transaction begin");
                let val = String::from("aaa").into_bytes();
                persy
                    .insert_record(&mut tx, "def", &val)
                    .expect("error on insert value");
                let fin = persy.prepare_commit(tx).expect("error on commit prepare");
                persy.commit(fin).expect("error on commit");
                count.count_down().expect("lock not panic");
            });
        }

        count.wait().expect("threas not finisced");

        let val = String::from("aaa").into_bytes();
        let mut cc = 0;
        for (_, content) in persy.scan("def").expect("error on scan") {
            assert_eq!(content, val);
            cc += 1;
        }
        assert_eq!(cc, 2);
    });
}

#[test]
pub fn councurrent_update_removed() {
    create_and_drop("concurrent_update_remove", |persy| {
        let mut tx = persy.begin().expect("error on transactoin begin");
        persy.create_segment(&mut tx, "def").expect("error on segment creation");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");

        let mut tx = persy.begin().expect("error on transaction begin");
        let val = String::from("aaa").into_bytes();
        let id = persy.insert_record(&mut tx, "def", &val).expect("error on inser value");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");

        let mut tx = persy.begin().expect("error on transaction begin");
        let val = String::from("cccc").into_bytes();
        persy
            .update_record(&mut tx, "def", &id, &val)
            .expect("error on update value");

        let count = Arc::new(CountDown::new(1));

        {
            let count = count.clone();
            let persy = persy.clone();
            let id = id.clone();
            thread::spawn(move || {
                let mut tx = persy.begin().expect("error on transaction begin");
                persy.delete_record(&mut tx, "def", &id).expect("error on delete value");
                let fin = persy.prepare_commit(tx).expect("error on commit prepare");
                persy.commit(fin).expect("error on commit");
                count.count_down().expect("lock not panic");
            });
        }

        count.wait().expect("threas not finisced");
        let fin = persy.prepare_commit(tx);
        assert!(fin.is_err());
    });
}

#[test]
pub fn test_recover_prepared_tx() {
    Persy::create("./target/test_recover_prepared.persy").unwrap();
    let id;
    let val;
    {
        let persy = Persy::open("./target/test_recover_prepared.persy", Config::new()).unwrap();
        let mut tx = persy.begin().expect("error on transactoin begin");
        persy.create_segment(&mut tx, "def").expect("error on segment creation");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");

        let mut tx = persy.begin().expect("error on transaction begin");
        val = String::from("aaa").into_bytes();
        id = persy
            .insert_record(&mut tx, "def", &val)
            .expect("error on insert value");
        let mut prepared = persy.prepare_commit(tx).expect("error on commit prepare");
        prepared.finalize.finished = true;
    }
    {
        let persy = Persy::open("./target/test_recover_prepared.persy", Config::new()).unwrap();
        assert_eq!(persy.read_record("def", &id).expect("error reading record"), Some(val));
    }

    fs::remove_file("./target/test_recover_prepared.persy").unwrap();
}

#[test]
pub fn test_dobule_recover_prepared_tx() {
    Persy::create("./target/test_double_recover_prepared.persy").unwrap();
    let id;
    let id1;
    let val;
    let val1;
    {
        let persy = Persy::open("./target/test_double_recover_prepared.persy", Config::new()).unwrap();
        let mut tx = persy.begin().expect("error on transactoin begin");
        persy.create_segment(&mut tx, "def").expect("error on segment creation");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");

        let mut tx = persy.begin().expect("error on transaction begin");
        val = String::from("aaa").into_bytes();
        id = persy
            .insert_record(&mut tx, "def", &val)
            .expect("error on insert value");
        let mut prepared = persy.prepare_commit(tx).expect("error on commit prepare");
        prepared.finalize.finished = true;
    }
    {
        let persy = Persy::open("./target/test_double_recover_prepared.persy", Config::new()).unwrap();
        assert_eq!(
            persy.read_record("def", &id).expect("error reading record"),
            Some(val.clone())
        );
        let mut tx = persy.begin().expect("error on transaction begin");
        val1 = String::from("bbbb").into_bytes();
        id1 = persy
            .insert_record(&mut tx, "def", &val1)
            .expect("error on insert value");
        let mut prepared = persy.prepare_commit(tx).expect("error on commit prepare");
        prepared.finalize.finished = true;
    }

    {
        let persy = Persy::open("./target/test_double_recover_prepared.persy", Config::new()).unwrap();
        assert_eq!(persy.read_record("def", &id).expect("error reading record"), Some(val));
        assert_eq!(
            persy.read_record("def", &id1).expect("error reading record",),
            Some(val1)
        );
    }
    fs::remove_file("./target/test_double_recover_prepared.persy").unwrap();
}

#[test]
pub fn test_recover_tx_id() {
    Persy::create("./target/test_recover_tx_id.persy").unwrap();
    let id;
    let val;
    let tx_id = vec![10; 5];
    {
        let persy = Persy::open("./target/test_recover_tx_id.persy", Config::new()).unwrap();
        let mut tx = persy.begin().expect("error on transactoin begin");
        persy.create_segment(&mut tx, "def").expect("error on segment creation");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");

        let mut tx = persy.begin_id(tx_id.clone()).expect("error on transaction begin");
        val = String::from("aaa").into_bytes();
        id = persy
            .insert_record(&mut tx, "def", &val)
            .expect("error on insert value");
        let mut prepared = persy.prepare_commit(tx).expect("error on commit prepare");
        prepared.finalize.finished = true;
    }
    {
        let persy = Persy::open_with_recover("./target/test_recover_tx_id.persy", Config::new(), |t_id| {
            assert_eq!(&tx_id, t_id);
            true
        })
        .unwrap();
        assert_eq!(persy.read_record("def", &id).expect("error reading record"), Some(val));
    }

    fs::remove_file("./target/test_recover_tx_id.persy").unwrap();
}

#[test]
#[allow(unused_must_use)]
pub fn test_rollback_prepared_tx() {
    Persy::create("./target/test_recover_rollback_prepared.persy").unwrap();
    let id;
    let val;
    {
        let persy = Persy::open("./target/test_recover_rollback_prepared.persy", Config::new()).unwrap();
        let mut tx = persy.begin().expect("error on transactoin begin");
        persy.create_segment(&mut tx, "def").expect("error on segment creation");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");

        let mut tx = persy.begin().expect("error on transaction begin");
        val = String::from("aaa").into_bytes();
        id = persy
            .insert_record(&mut tx, "def", &val)
            .expect("error on insert value");
        persy.prepare_commit(tx).expect("error on commit prepare");
    }
    {
        let persy = Persy::open_with_recover("./target/test_recover_rollback_prepared.persy", Config::new(), |_| {
            false
        })
        .unwrap();
        assert_eq!(persy.read_record("def", &id).expect("error reading record"), None);
    }

    fs::remove_file("./target/test_recover_rollback_prepared.persy").unwrap();
}

#[test]
#[allow(unused_must_use)]
pub fn test_autorollback_lost_finalize() {
    Persy::create("./target/test_auto_rollback.persy").unwrap();
    let id;
    {
        let persy = Persy::open("./target/test_auto_rollback.persy", Config::new()).unwrap();
        let mut tx = persy.begin().expect("error on transaction begin");
        persy.create_segment(&mut tx, "def").expect("error on segment creation");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");

        let mut tx = persy.begin().expect("error on transaction begin");
        let val = String::from("aaa").into_bytes();
        id = persy
            .insert_record(&mut tx, "def", &val)
            .expect("error on insert value");
        persy.prepare_commit(tx).expect("error on commit prepare");
    }
    {
        let persy = Persy::open("./target/test_auto_rollback.persy", Config::new()).unwrap();
        assert_eq!(persy.read_record("def", &id).expect("error reading record"), None);
    }

    fs::remove_file("./target/test_auto_rollback.persy").unwrap();
}

#[test]
pub fn test_recover_stale_tx() {
    Persy::create("./target/test_recover_stale.persy").unwrap();
    let id;
    {
        let persy = Persy::open("./target/test_recover_stale.persy", Config::new()).unwrap();
        let mut tx = persy.begin().expect("error on transactoin begin");
        persy.create_segment(&mut tx, "def").expect("error on segment creation");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");

        let mut tx = persy.begin().expect("error on transaction begin");
        let val = String::from("aaa").into_bytes();
        id = persy
            .insert_record(&mut tx, "def", &val)
            .expect("error on insert value");
    }
    {
        let persy = Persy::open("./target/test_recover_stale.persy", Config::new()).unwrap();
        assert_eq!(persy.read_record("def", &id).expect("error reading record"), None);
    }

    fs::remove_file("./target/test_recover_stale.persy").unwrap();
}

#[test]
pub fn test_multiple_open_tx_close() {
    let file = Builder::new()
        .prefix("multiple_open_tx_close")
        .suffix(".persy")
        .tempfile()
        .expect("expect temp file creation");
    Persy::create_from_file(file.reopen().unwrap()).unwrap();
    {
        let persy = Persy::open_from_file(file.reopen().unwrap(), Config::new()).unwrap();
        let mut tx = persy.begin().expect("error on transaction begin");
        persy.create_segment(&mut tx, "def").expect("error on segment creation");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");
    }
    for ite in 1..10 {
        let persy = Persy::open_from_file(file.reopen().unwrap(), Config::new()).unwrap();
        let mut tx = persy.begin().expect("error on transaction begin");
        let val = String::from("aaa").into_bytes();
        persy
            .insert_record(&mut tx, "def", &val)
            .expect("error on insert value");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");
        let mut counter = 0;
        for _ in persy.scan("def").expect("read persistent records ") {
            counter += 1;
        }
        assert_eq!(ite, counter);
    }
}
