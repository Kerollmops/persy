mod helpers;
use helpers::create_and_drop;
use persy::Persy;
use persy::{ByteVec, IndexIter, IndexType, PRes, TxIndexIter, Value, ValueMode};

fn create_and_drop_index<F>(test: &str, f: F)
where
    F: FnOnce(&Persy, &str),
{
    create_and_drop_index_mode(test, ValueMode::CLUSTER, f);
}

fn create_and_drop_index_mode<F>(test: &str, mode: ValueMode, f: F)
where
    F: FnOnce(&Persy, &str),
{
    create_and_drop(test, |persy| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .create_index::<u8, u8>(&mut tx, "index1", mode)
            .expect("index created correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        f(persy, "index1");

        let mut tx = persy.begin().expect("begin transaction works");
        persy.drop_index(&mut tx, "index1").expect("index created correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");
    });
}

#[test]
fn test_create_drop_index() {
    create_and_drop("create_drop_index", |persy| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .create_index::<u8, u8>(&mut tx, "index1", ValueMode::CLUSTER)
            .expect("index created correctly");
        assert!(persy.exists_index_tx(&mut tx, "index1").expect("exists wokrs"));
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");
        assert!(persy.exists_index("index1").expect("exists wokrs"));

        let mut tx = persy.begin().expect("begin transaction works");
        persy.drop_index(&mut tx, "index1").expect("index created correctly");
        assert!(!persy.exists_index_tx(&mut tx, "index1").expect("exists wokrs"));
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");
        assert!(!persy.exists_index("index1").expect("exists wokrs"));
    });
}

#[test]
fn test_create_put_remove_get_drop_index_same_tx() {
    create_and_drop("create_drop_index", |persy| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .create_index::<u8, u8>(&mut tx, "index1", ValueMode::CLUSTER)
            .expect("index created correctly");
        persy
            .put::<u8, u8>(&mut tx, "index1", 10, 12)
            .expect("put works correctly");
        let res = persy
            .get_tx::<u8, u8>(&mut tx, "index1", &10)
            .expect("get works correctly");
        assert_eq!(Some(Value::SINGLE(12)), res);
        persy
            .remove::<u8, u8>(&mut tx, "index1", 10, None)
            .expect("put works correctly");
        let res = persy
            .get_tx::<u8, u8>(&mut tx, "index1", &10)
            .expect("get works correctly");
        assert_eq!(None, res);

        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");
        let mut tx = persy.begin().expect("begin transaction works");
        persy.drop_index(&mut tx, "index1").expect("index created correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");
    });
}

fn test_for_type<K: IndexType, V>(persy: &Persy, name: &str, k: K, v: V) -> PRes<()>
where
    V: IndexType + std::fmt::Debug + PartialEq,
{
    let mut tx = persy.begin()?;
    persy.create_index::<K, V>(&mut tx, name, ValueMode::CLUSTER)?;
    persy.put::<K, V>(&mut tx, name, k.clone(), v.clone())?;
    let res = persy.get_tx::<K, V>(&mut tx, name, &k)?;
    assert_eq!(Some(Value::SINGLE(v.clone())), res);
    let prep = persy.prepare_commit(tx)?;
    persy.commit(prep)?;

    let res = persy.get::<K, V>(name, &k)?;
    assert_eq!(Some(Value::SINGLE(v)), res);
    let mut tx = persy.begin()?;
    persy.remove::<K, V>(&mut tx, name, k.clone(), None)?;
    let res = persy.get_tx::<K, V>(&mut tx, name, &k)?;
    assert!(res.is_none());
    let prep = persy.prepare_commit(tx)?;
    persy.commit(prep)?;

    let res = persy.get::<K, V>(name, &k)?;
    assert!(res.is_none());
    let mut tx = persy.begin()?;
    persy.drop_index(&mut tx, name)?;
    let prep = persy.prepare_commit(tx)?;
    persy.commit(prep)?;
    Ok(())
}

#[test]
fn test_all_indexable_types() {
    create_and_drop("all_indexable_types", |persy| {
        test_for_type::<u8, u8>(persy, "idx_u8", 10, 10).expect("test pass");
        test_for_type::<u16, u16>(persy, "idx_u16", 10, 10).expect("test pass");
        test_for_type::<u32, u32>(persy, "idx_u32", 10, 10).expect("test pass");
        test_for_type::<u64, u64>(persy, "idx_u64", 10, 10).expect("test pass");
        test_for_type::<u128, u128>(persy, "idx_u128", 10, 10).expect("test pass");
        test_for_type::<i8, i8>(persy, "idx_i8", 10, 10).expect("test pass");
        test_for_type::<i16, i16>(persy, "idx_i16", 10, 10).expect("test pass");
        test_for_type::<i32, i32>(persy, "idx_i32", 10, 10).expect("test pass");
        test_for_type::<i64, i64>(persy, "idx_i64", 10, 10).expect("test pass");
        test_for_type::<i128, i128>(persy, "idx_i128", 10, 10).expect("test pass");
        test_for_type::<f32, f32>(persy, "idx_f32", 10.0, 10.0).expect("test pass");
        test_for_type::<f64, f64>(persy, "idx_f64", 10.0, 10.0).expect("test pass");
        test_for_type::<String, String>(persy, "idx_string", "key".to_string(), "value".to_string())
            .expect("test pass");
        test_for_type::<ByteVec, ByteVec>(persy, "idx_bytevec", vec![10; 10].into(), vec![10; 10].into())
            .expect("test pass");
    });
}

#[test]
fn test_put_get_index() {
    create_and_drop_index("create_drop_index", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect("get works correctly");
        assert_eq!(res, Some(Value::SINGLE(12)));
    });
}

#[test]
fn test_put_get_tx_index() {
    create_and_drop_index("create_drop_index", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        let res = persy
            .get_tx::<u8, u8>(&mut tx, index_name, &10)
            .expect("get works correctly");
        assert_eq!(res, Some(Value::SINGLE(12)));
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect("get works correctly");
        assert_eq!(res, Some(Value::SINGLE(12)));
    });
}

#[test]
fn test_put_remove_index() {
    create_and_drop_index("create_drop_index", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect("get works correctly");
        assert_eq!(res, Some(Value::SINGLE(12)));

        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .remove::<u8, u8>(&mut tx, index_name, 10, None)
            .expect("put works correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect("get works correctly");
        assert_eq!(res, None);
    });
}

#[test]
fn test_duplicate_put() {
    create_and_drop_index_mode("create_drop_index", ValueMode::EXCLUSIVE, |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 20)
            .expect("put works correctly");
        assert!(persy.prepare_commit(tx).is_err());
    });
}

#[test]
fn test_same_value_no_duplicate_put() {
    create_and_drop_index_mode("create_drop_index", ValueMode::EXCLUSIVE, |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect("get works correctly");
        assert_eq!(res, Some(Value::SINGLE(12)));
    });
}

#[test]
fn test_duplicate_second_put() {
    create_and_drop_index_mode("create_drop_index", ValueMode::EXCLUSIVE, |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");

        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 20)
            .expect("put works correctly");
        assert!(persy.prepare_commit(tx).is_err());
    });
}

#[test]
fn test_duplicate_tx_put() {
    create_and_drop_index_mode("create_drop_index", ValueMode::EXCLUSIVE, |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 20)
            .expect("put works correctly");
        assert!(persy.get_tx::<u8, u8>(&mut tx, index_name, &10).is_err());
    });
}

#[test]
fn test_same_value_no_duplicate_put_tx() {
    create_and_drop_index_mode("create_drop_index", ValueMode::EXCLUSIVE, |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        assert_eq!(
            persy.get_tx::<u8, u8>(&mut tx, index_name, &10),
            Ok(Some(Value::SINGLE(12)))
        );
    });
}

#[test]
fn test_put_remove_index_one_tx() {
    create_and_drop_index("create_drop_index", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        persy
            .remove::<u8, u8>(&mut tx, index_name, 10, None)
            .expect("put works correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect("get works correctly");
        assert_eq!(res, None);
    });
}

#[test]
fn test_mupltiple_values_put_get() {
    create_and_drop_index("create_drop_index", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");

        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 14)
            .expect("put works correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect("get works correctly");
        assert_eq!(res, Some(Value::CLUSTER(vec![12, 14])));
    });
}

#[test]
fn test_mupltiple_values_put_get_tx() {
    create_and_drop_index("create_drop_index", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");

        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 14)
            .expect("put works correctly");
        let res = persy
            .get_tx::<u8, u8>(&mut tx, index_name, &10)
            .expect("get works correctly");
        assert_eq!(res, Some(Value::CLUSTER(vec![12, 14])));
    });
}

#[test]
fn test_mupltiple_put_same_value_get() {
    create_and_drop_index("create_drop_index", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");

        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect("get works correctly");
        assert_eq!(res, Some(Value::SINGLE(12)));
    });
}

#[test]
fn test_index_browse() {
    create_and_drop_index("test_index_browse", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        for n in 10..30 {
            persy
                .put::<u8, u8>(&mut tx, index_name, n, 12)
                .expect("put works correctly");
        }

        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let start = 10;
        let to = 29;
        let mut count = to - start;
        let mut iter: IndexIter<u8, u8> = persy.range(index_name, ..).expect("browse works correctly");
        assert_eq!(iter.next().unwrap().0, start);
        let mut last = None;
        for x in iter {
            last = Some(x.0.clone());
            assert_eq!(x.1, Value::SINGLE(12));
            count -= 1;
        }
        assert_eq!(Some(to), last);
        assert_eq!(0, count);
    });
}

#[test]
fn test_index_range() {
    create_and_drop_index("test_index_range", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        for n in 10..30 {
            persy
                .put::<u8, u8>(&mut tx, index_name, n, 12)
                .expect("put works correctly");
        }

        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let start = 13;
        let to = 24;
        let mut count = to - start;
        let mut iter: IndexIter<u8, u8> = persy.range(index_name, start..=to).expect("range works correctly");
        assert_eq!(iter.next().unwrap().0, start);
        let mut last = None;
        for x in iter {
            last = Some(x.0.clone());
            assert_eq!(x.1, Value::SINGLE(12));
            count -= 1;
        }
        assert_eq!(Some(to), last);
        assert_eq!(0, count);
    });
}

#[test]
fn test_index_range_rev() {
    create_and_drop_index("test_index_range_rev", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        for n in 10..30 {
            persy
                .put::<u8, u8>(&mut tx, index_name, n, 12)
                .expect("put works correctly");
        }

        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let start = 13;
        let to = 24;
        let mut count = to - start;
        let base: IndexIter<u8, u8> = persy.range(index_name, start..=to).expect("range works correctly");
        let mut iter = base.rev();
        assert_eq!(iter.next().unwrap().0, to);
        let mut last = None;
        for x in iter {
            last = Some(x.0.clone());
            assert_eq!(x.1, Value::SINGLE(12));
            count -= 1;
        }
        assert_eq!(Some(start), last);
        assert_eq!(0, count);
    });
}

#[test]
fn test_index_browse_tx() {
    create_and_drop_index("test_index_browse_tx", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        for n in 10..30 {
            persy
                .put::<u8, u8>(&mut tx, index_name, n, 12)
                .expect("put works correctly");
        }
        let start = 10;
        let to = 29;
        let mut count = to - start;
        let mut last = None;
        {
            let mut iter: TxIndexIter<u8, u8> =
                persy.range_tx(&mut tx, index_name, ..).expect("browse works correctly");
            assert_eq!(iter.next().unwrap().0, start);
            for x in iter {
                last = Some(x.0.clone());
                assert_eq!(x.1, Value::SINGLE(12));
                count -= 1;
            }
        }
        assert_eq!(Some(to), last);
        assert_eq!(0, count);
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");
    });
}

#[test]
fn test_index_range_tx() {
    create_and_drop_index("test_index_range_tx", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        for n in 10..30 {
            persy
                .put::<u8, u8>(&mut tx, index_name, n, 12)
                .expect("put works correctly");
        }

        let start = 13;
        let to = 24;
        let mut count = to - start;
        {
            let mut iter: TxIndexIter<u8, u8> = persy
                .range_tx(&mut tx, index_name, start..=to)
                .expect("range works correctly");
            assert_eq!(iter.next().unwrap().0, start);
            let mut last = None;
            for x in iter {
                last = Some(x.0.clone());
                assert_eq!(x.1, Value::SINGLE(12));
                count -= 1;
            }
            assert_eq!(Some(to), last);
            assert_eq!(0, count);
        }

        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");
    });
}

#[test]
fn test_index_range_tx_access() {
    create_and_drop_index("test_index_range_tx_access", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy.create_segment(&mut tx, "seg").expect("create segment");
        for n in 10..30 {
            persy
                .put::<u8, u8>(&mut tx, index_name, n, 12)
                .expect("put works correctly");
        }

        let start = 13;
        let to = 24;
        let mut count = to - start;
        {
            let mut iter: TxIndexIter<u8, u8> = persy
                .range_tx(&mut tx, index_name, start..=to)
                .expect("range works correctly");
            assert_eq!(iter.next().unwrap().0, start);
            persy
                .insert_record(iter.tx(), "seg", "data".as_bytes())
                .expect("insert works");
            let mut last = None;
            while let Some(x) = iter.next_tx() {
                last = Some(x.0.clone());
                assert_eq!(x.1, Value::SINGLE(12));
                persy
                    .insert_record(x.2, "seg", "data".as_bytes())
                    .expect("insert works");
                count -= 1;
            }
            assert_eq!(Some(to), last);
            assert_eq!(0, count);
        }

        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");
    });
}

#[test]
fn test_index_range_tx_rev() {
    create_and_drop_index("test_index_range_tx_rev", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        for n in 10..30 {
            persy
                .put::<u8, u8>(&mut tx, index_name, n, 12)
                .expect("put works correctly");
        }

        let start = 13;
        let to = 24;
        let mut count = to - start;
        {
            let base: TxIndexIter<u8, u8> = persy
                .range_tx(&mut tx, index_name, start..=to)
                .expect("range works correctly");
            let mut iter = base.rev();
            assert_eq!(iter.next().unwrap().0, to);
            let mut last = None;
            for x in iter {
                last = Some(x.0.clone());
                assert_eq!(x.1, Value::SINGLE(12));
                count -= 1;
            }
            assert_eq!(Some(start), last);
            assert_eq!(0, count);
        }

        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");
    });
}

#[test]
fn test_mupltiple_put_same_value_get_tx() {
    create_and_drop_index("create_drop_index", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");

        persy
            .put::<u8, u8>(&mut tx, index_name, 10, 12)
            .expect("put works correctly");
        let res = persy
            .get_tx::<u8, u8>(&mut tx, index_name, &10)
            .expect("get works correctly");
        assert_eq!(res, Some(Value::SINGLE(12)));
    });
}

#[test]
pub fn test_list_indexes() {
    create_and_drop("test_list_indexes", |persy| {
        let mut tx = persy.begin().expect("error on transaction begin");
        persy
            .create_index::<u8, u8>(&mut tx, "one", ValueMode::REPLACE)
            .expect("error on index creation");
        persy
            .create_index::<u32, u32>(&mut tx, "two", ValueMode::REPLACE)
            .expect("error on index creation");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");
        let indexes = persy.list_indexes().expect("list indexes works as expected");
        assert_eq!(indexes.len(), 2);
        let names = indexes.into_iter().map(|x| x.0).collect::<Vec<String>>();
        assert!(names.contains(&"one".to_string()));
        assert!(names.contains(&"two".to_string()));
        let segments = persy.list_segments().expect("list segments works as expected");
        assert_eq!(segments.len(), 0);
    });
}

#[test]
pub fn test_list_indexes_tx() {
    create_and_drop("test_list_indexes", |persy| {
        let mut tx = persy.begin().expect("error on transaction begin");
        persy
            .create_index::<u8, u8>(&mut tx, "one", ValueMode::REPLACE)
            .expect("error on index creation");
        persy
            .create_index::<u32, u32>(&mut tx, "two", ValueMode::REPLACE)
            .expect("error on index creation");
        let fin = persy.prepare_commit(tx).expect("error on commit prepare");
        persy.commit(fin).expect("error on commit");
        let mut tx = persy.begin().expect("error on transaction begin");
        persy
            .create_index::<u8, u8>(&mut tx, "three", ValueMode::REPLACE)
            .expect("error on index creation");
        persy.drop_index(&mut tx, "two").expect("error on index creation");

        let indexes = persy.list_indexes_tx(&mut tx).expect("list indexes works as expected");
        assert_eq!(indexes.len(), 2);
        let names = indexes.into_iter().map(|x| x.0).collect::<Vec<String>>();
        assert!(names.contains(&"one".to_string()));
        assert!(names.contains(&"three".to_string()));

        let segments = persy
            .list_segments_tx(&mut tx)
            .expect("list segments works as expected");
        assert_eq!(segments.len(), 0);
    });
}
