
# TO DO
In case of drop segment transform in delete all the create/update record operation in transaction for the relative segment
In case of recover make sure that none of the pages recovered are present in the free list
Introduce some tracking and retain in memory of written pages to re-apply them in case of fsync failure.
Implement advanced caching algorithms
Manage shrink of file, think to improve the algorithm for release pages
Introduce a new type of "IntoPersyStructureId" that source from both PersyStructureId and &str, so the same api can be used with optimizedIds and not optimal &str
Make sure in case of lock acquire failure to release all the already acquired locks, and only the acquired one, a double release could cause unlocking of locks taken by other transactions

## Features

# Tests TO DO

## Multithread
manipolate record of segment while another thread drop the segment

## Allocation
Add test case for check if disk space is returned correctly after drop segment

## Scan Transactions
Test failure of scan of a segment created in another tx

# Code cleanup
check for in tests module refer and remove double implementation of CountDown

# Optimizations
Remove segment check in prepare-commit record locks



