//! # Persy - Transactional Persistence Engine
//!
//! Simple single file, durable, paginated, transactional persistence engine, based on copy on write, write
//! ahead log, two phase commit.
//!
//! # Example
//!
//! ```rust
//! use persy::{Persy,Config};
//! //...
//! # use persy::PRes;
//! # fn foo() -> PRes<()> {
//! Persy::create("./open.persy")?;
//! let persy = Persy::open("./open.persy",Config::new())?;
//! let mut tx = persy.begin()?;
//! persy.create_segment(&mut tx, "seg")?;
//! let data = vec![1;20];
//! persy.insert_record(&mut tx, "seg", &data)?;
//! let prepared = persy.prepare_commit(tx)?;
//! persy.commit(prepared)?;
//! for (_id,content) in persy.scan("seg")? {
//!     assert_eq!(content[0], 20);
//!     //....
//! }
//! # Ok(())
//! # }
//! ```
//!
//!

mod address;
mod allocator;
mod config;
mod discref;
mod flush_checksum;
mod index;
mod journal;
mod locks;
mod persy;
mod record_scanner;
mod segment;
mod snapshot;
mod transaction;

pub use crate::persy::{IndexInfo, PRes, PersyError};
use crate::persy::{PersyImpl, TxFinalize};
pub use config::{Config, TxStrategy};
pub use index::config::{ByteVec, IndexType, IndexTypeId, ValueMode};
use index::keeper::{IndexRawIter, TxIndexRawIter};
pub use index::tree::Value;
#[allow(deprecated)]
pub use record_scanner::{IteratorItem, RecordScanner, RecordScannerTx};
use record_scanner::{SegmentRawIter, TxSegmentRawIter};
use std::{fmt, fs::File, ops::RangeBounds, path::Path, sync::Arc};

/// Identifier of a persistent record, can be used for read, update or delete the record
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct PersyId(persy::RecRef);
impl fmt::Display for PersyId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::str::FromStr for PersyId {
    type Err = PersyError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(PersyId(s.parse()?))
    }
}

/// Transaction container, it include all the changes done in a transaction.
pub struct Transaction(transaction::Transaction);
/// Unique identifier of an index
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct IndexId(u32);
/// Unique identifier of a segment
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Debug)]
pub struct SegmentId(u32);
pub type TransactionId = Vec<u8>;

/// Main structure to operate persy storage files
///
#[derive(Clone)]
pub struct Persy {
    persy_impl: Arc<PersyImpl>,
}

/// prepared transaction state
#[must_use]
pub struct TransactionFinalize {
    persy_impl: Arc<PersyImpl>,
    pub finalize: TxFinalize,
}

impl Drop for TransactionFinalize {
    fn drop(&mut self) {
        self.persy_impl.rollback_prepared(&mut self.finalize).unwrap();
    }
}

/// Index Iterator implementation for iterating on a range of keys
pub struct IndexIter<K: IndexType, V: IndexType> {
    iter_impl: IndexRawIter<K, V>,
    persy_impl: Arc<PersyImpl>,
}

impl<K: IndexType, V: IndexType> IndexIter<K, V> {
    fn new(iter_impl: IndexRawIter<K, V>, persy_impl: Arc<PersyImpl>) -> IndexIter<K, V> {
        IndexIter { iter_impl, persy_impl }
    }
}

impl<K, V> Iterator for IndexIter<K, V>
where
    K: IndexType,
    V: IndexType,
{
    type Item = (K, Value<V>);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter_impl.next(&self.persy_impl)
    }
}
impl<K, V> DoubleEndedIterator for IndexIter<K, V>
where
    K: IndexType,
    V: IndexType,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        self.iter_impl.next_back(&self.persy_impl)
    }
}

impl<K, V> Drop for IndexIter<K, V>
where
    K: IndexType,
    V: IndexType,
{
    fn drop(&mut self) {
        self.iter_impl.release(&self.persy_impl).unwrap()
    }
}

/// Index Iterator implementation for iterating on a range of keys
pub struct TxIndexIter<'a, K: IndexType, V: IndexType> {
    iter_impl: TxIndexRawIter<K, V>,
    persy_impl: Arc<PersyImpl>,
    tx: &'a mut Transaction,
}

impl<'a, K: IndexType, V: IndexType> TxIndexIter<'a, K, V> {
    fn new(
        iter_impl: TxIndexRawIter<K, V>,
        persy_impl: Arc<PersyImpl>,
        tx: &'a mut Transaction,
    ) -> TxIndexIter<'a, K, V> {
        TxIndexIter {
            iter_impl,
            persy_impl,
            tx,
        }
    }

    /// get the next element in the iterator giving the access on the transaction owned by the
    /// iterator
    pub fn next_tx(&mut self) -> Option<(K, Value<V>, &mut Transaction)> {
        if let Some((k, v)) = self.iter_impl.next(&self.persy_impl, &mut self.tx.0) {
            Some((k, v, self.tx))
        } else {
            None
        }
    }

    /// Direct access to the transaction owned by the iterator
    pub fn tx(&mut self) -> &mut Transaction {
        self.tx
    }
}

impl<'a, K, V> Iterator for TxIndexIter<'a, K, V>
where
    K: IndexType,
    V: IndexType,
{
    type Item = (K, Value<V>);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter_impl.next(&self.persy_impl, &mut self.tx.0)
    }
}

impl<'a, K, V> DoubleEndedIterator for TxIndexIter<'a, K, V>
where
    K: IndexType,
    V: IndexType,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        self.iter_impl.next_back(&self.persy_impl, &mut self.tx.0)
    }
}

/// Iterator implementation to scan a segment
pub struct SegmentIter {
    iter_impl: SegmentRawIter,
    persy_impl: Arc<PersyImpl>,
}

impl SegmentIter {
    fn new(iter_impl: SegmentRawIter, persy_impl: Arc<PersyImpl>) -> SegmentIter {
        SegmentIter { iter_impl, persy_impl }
    }
}

impl Iterator for SegmentIter {
    type Item = (PersyId, Vec<u8>);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter_impl.next(&self.persy_impl)
    }
}

/// Iterator implementation to scan a segment considering in transaction changes.
pub struct TxSegmentIter<'a> {
    iter_impl: TxSegmentRawIter,
    persy_impl: Arc<PersyImpl>,
    tx: &'a mut Transaction,
}

impl<'a> TxSegmentIter<'a> {
    fn new(iter_impl: TxSegmentRawIter, persy_impl: Arc<PersyImpl>, tx: &'a mut Transaction) -> TxSegmentIter<'a> {
        TxSegmentIter {
            iter_impl,
            persy_impl,
            tx,
        }
    }

    /// get the next element in the iterator giving the access on the transaction owned by the
    /// iterator
    pub fn next_tx(&mut self) -> Option<(PersyId, Vec<u8>, &mut Transaction)> {
        if let Some((id, rec)) = self.iter_impl.next(&self.persy_impl, &mut self.tx.0) {
            Some((id, rec, self.tx))
        } else {
            None
        }
    }

    /// Direct access to the transaction owned by the iterator
    pub fn tx(&mut self) -> &mut Transaction {
        self.tx
    }
}

impl<'a> Iterator for TxSegmentIter<'a> {
    type Item = (PersyId, Vec<u8>);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter_impl.next(&self.persy_impl, &mut self.tx.0)
    }
}

impl Persy {
    /// Create a new database file.
    ///
    /// # Errors
    /// if the file already exist fail.
    ///
    pub fn create<P: AsRef<Path>>(path: P) -> PRes<()> {
        PersyImpl::create(path.as_ref())
    }

    /// Create a new database file.
    ///
    /// # Errors
    /// if the file already exist fail.
    ///
    pub fn create_from_file(file: File) -> PRes<()> {
        PersyImpl::create_from_file(file)
    }

    /// Open a database file.
    ///
    /// The file should have been created with [`Persy::create`]
    ///
    ///
    /// # Errors
    ///
    /// if the file does not exist fail.
    ///
    /// [`Persy::create`]: struct.Persy.html#method.create
    pub fn open<P: AsRef<Path>>(path: P, config: Config) -> PRes<Persy> {
        Ok(Persy {
            persy_impl: Arc::new(PersyImpl::open(path.as_ref(), config)?),
        })
    }

    /// Open a database file from a path with a recover function.
    ///
    /// The file should have been created with [`Persy::create`]
    ///
    ///
    /// # Errors
    ///
    /// if the file does not exist fail.
    ///
    /// [`Persy::create`]: struct.Persy.html#method.create
    pub fn open_with_recover<P: AsRef<Path>, C>(path: P, config: Config, recover: C) -> PRes<Persy>
    where
        C: Fn(&TransactionId) -> bool,
    {
        Ok(Persy {
            persy_impl: Arc::new(PersyImpl::open_with_recover(path.as_ref(), config, recover)?),
        })
    }

    /// Open a database file from a direct file handle.
    ///
    /// The file should have been created with [`Persy::create`]
    ///
    ///
    /// # Errors
    ///
    /// if the file does not exist fail.
    ///
    /// [`Persy::create`]: struct.Persy.html#method.create
    pub fn open_from_file(path: File, config: Config) -> PRes<Persy> {
        Ok(Persy {
            persy_impl: Arc::new(PersyImpl::open_from_file(path, config)?),
        })
    }

    /// Open a database file, from a direct file handle and a transaction recover function.
    ///
    /// The file should have been created with [`Persy::create`]
    ///
    ///
    /// # Errors
    ///
    /// if the file does not exist fail.
    ///
    /// [`Persy::create`]: struct.Persy.html#method.create
    pub fn open_from_file_with_recover<C>(path: File, config: Config, recover: C) -> PRes<Persy>
    where
        C: Fn(&TransactionId) -> bool,
    {
        Ok(Persy {
            persy_impl: Arc::new(PersyImpl::open_from_file_with_recover(path, config, recover)?),
        })
    }

    /// Open an existing database or create it if it does not exist yet,
    /// calling the `prepare` function just after the creation.
    ///
    /// ```rust
    /// use std::path::Path;
    /// use persy::{Persy, Config, PersyId, ValueMode};
    /// # use persy::PRes;
    ///
    /// # fn main() -> PRes<()> {
    /// let path = Path::new("target/persy.db");
    /// let config = Config::new();
    ///
    /// let persy = Persy::open_or_create_with(path, config, |persy| {
    ///     // this closure is only called on database creation
    ///     let mut tx = persy.begin()?;
    ///     persy.create_segment(&mut tx, "data")?;
    ///     persy.create_index::<u64, PersyId>(&mut tx, "index", ValueMode::REPLACE)?;
    ///     let prepared = persy.prepare_commit(tx)?;
    ///     persy.commit(prepared)?;
    ///     println!("Segment and Index successfully created");
    ///     Ok(())
    /// })?;
    /// # std::fs::remove_file("target/persy.db")?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn open_or_create_with<P, F>(path: P, config: Config, prepare: F) -> PRes<Persy>
    where
        P: AsRef<Path>,
        F: FnOnce(&Persy) -> PRes<()>,
    {
        let path = path.as_ref();
        let persy;

        if !path.exists() {
            Persy::create(path)?;
            persy = Persy::open(path, config)?;
            prepare(&persy)?;
        } else {
            persy = Persy::open(path, config)?;
        }

        Ok(persy)
    }

    /// Begin a new transaction.
    ///
    /// The transaction isolation level is 'read_commited'.
    /// for commit call [`prepare_commit`] and [`commit`]
    ///
    /// [`prepare_commit`]:struct.Persy.html#method.prepare_commit
    /// [`commit`]:struct.Persy.html#method.commit
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// // ...
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn begin(&self) -> PRes<Transaction> {
        Ok(Transaction(self.persy_impl.begin()?))
    }

    /// Begin a new transaction specifying and id usable for crash recover.
    ///
    /// The transaction isolation level is 'read_commited'.
    /// for commit call [`prepare_commit`] and [`commit`]
    ///
    /// [`prepare_commit`]:struct.Persy.html#method.prepare_commit
    /// [`commit`]:struct.Persy.html#method.commit
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let tx_id = vec![2;2];
    /// let mut tx = persy.begin_id(tx_id)?;
    /// // ...
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn begin_id(&self, meta_id: TransactionId) -> PRes<Transaction> {
        Ok(Transaction(self.persy_impl.begin_id(meta_id)?))
    }

    /// Create a new segment with the provided name
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_segment(&mut tx, "my_new_segment")?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn create_segment(&self, tx: &mut Transaction, segment: &str) -> PRes<()> {
        assert!(!segment.starts_with(index::config::INDEX_META_PREFIX));
        assert!(!segment.starts_with(index::config::INDEX_DATA_PREFIX));
        self.persy_impl.create_segment(&mut tx.0, segment)
    }

    /// Drop a existing segment
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.drop_segment(&mut tx, "existing_segment_name")?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    ///
    pub fn drop_segment(&self, tx: &mut Transaction, segment: &str) -> PRes<()> {
        self.persy_impl.drop_segment(&mut tx.0, segment)
    }

    /// Check if a segment already exist in the storage
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_segment(&mut tx, "my_new_segment")?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// assert!(persy.exists_segment("my_new_segment")?);
    /// # Ok(())
    /// # }
    /// ```
    ///
    pub fn exists_segment(&self, segment: &str) -> PRes<bool> {
        self.persy_impl.exists_segment(segment)
    }

    /// Check if a segment already exist in the storage considering the transaction
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_segment(&mut tx, "my_new_segment")?;
    /// assert!(persy.exists_segment_tx(&mut tx, "my_new_segment")?);
    /// # let prepared = persy.prepare_commit(tx)?;
    /// # persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn exists_segment_tx(&self, tx: &Transaction, segment: &str) -> PRes<bool> {
        self.persy_impl.exists_segment_tx(&tx.0, segment)
    }

    /// create a new record
    ///
    /// This function return an id that can be used by [`read_record`] and [`read_record_tx`],
    /// the record content can be read only with the [`read_record_tx`] till the transaction is committed.
    ///
    /// [`read_record_tx`]:struct.Persy.html#method.read_record_tx
    /// [`read_record`]:struct.Persy.html#method.read_record
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// persy.insert_record(&mut tx, "seg", &data)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn insert_record(&self, tx: &mut Transaction, segment: &str, rec: &[u8]) -> PRes<PersyId> {
        Ok(PersyId(self.persy_impl.insert_record(&mut tx.0, segment, rec)?))
    }

    /// Read the record content considering eventual in transaction changes.
    ///
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let read = persy.read_record_tx(&mut tx, "seg", &id)?.expect("record exists");
    /// assert_eq!(data,read);
    /// # let prepared = persy.prepare_commit(tx)?;
    /// # persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn read_record_tx(&self, tx: &mut Transaction, segment: &str, id: &PersyId) -> PRes<Option<Vec<u8>>> {
        self.persy_impl.read_record_tx(&mut tx.0, segment, &id.0)
    }

    /// Read the record content from persistent data.
    ///
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// let read = persy.read_record("seg", &id)?.expect("record exits");
    /// assert_eq!(data,read);
    /// # Ok(())
    /// # }
    /// ```
    pub fn read_record(&self, segment: &str, id: &PersyId) -> PRes<Option<Vec<u8>>> {
        self.persy_impl.read_record(segment, &id.0)
    }

    /// Scan for persistent and in transaction records
    ///
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let mut count = 0;
    /// for found in persy.scan_records_tx(&mut tx, "seg")? {
    ///     println!("record size:{}",found.content.len());
    ///     count+=1;
    /// }
    /// assert_eq!(count,1);
    /// # Ok(())
    /// # }
    /// ```
    #[deprecated(since = "0.5.0", note = "please use `scan_tx` instead")]
    #[allow(deprecated)]
    pub fn scan_records_tx<'a>(&self, tx: &'a mut Transaction, segment: &str) -> PRes<RecordScannerTx<'a>> {
        Ok(RecordScannerTx::new(self.scan_tx(tx, segment)?))
    }

    /// Scan for persistent and in transaction records
    ///
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let mut count = 0;
    /// for (id,content) in persy.scan_tx(&mut tx, "seg")? {
    ///     println!("record size:{}",content.len());
    ///     count+=1;
    /// }
    /// assert_eq!(count,1);
    /// # Ok(())
    /// # }
    /// ```
    pub fn scan_tx<'a>(&self, tx: &'a mut Transaction, segment: &str) -> PRes<TxSegmentIter<'a>> {
        Ok(TxSegmentIter::new(
            self.persy_impl.scan_tx(&mut tx.0, segment)?,
            self.persy_impl.clone(),
            tx,
        ))
    }

    /// Scan a segment for persistent records
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// let mut count = 0;
    /// for found in persy.scan_records("seg")? {
    ///     println!("record size:{}",found.content.len());
    ///     count+=1;
    /// }
    /// assert_eq!(count,1);
    /// # Ok(())
    /// # }
    /// ```
    ///
    #[deprecated(since = "0.5.0", note = "please use `scan` instead")]
    #[allow(deprecated)]
    pub fn scan_records(&self, segment: &str) -> PRes<RecordScanner> {
        Ok(RecordScanner::new(self.scan(segment)?))
    }

    /// Scan a segment for persistent records
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// let mut count = 0;
    /// for (id,content) in persy.scan("seg")? {
    ///     println!("record size:{}",content.len());
    ///     count+=1;
    /// }
    /// assert_eq!(count,1);
    /// # Ok(())
    /// # }
    /// ```
    ///
    pub fn scan(&self, segment: &str) -> PRes<SegmentIter> {
        Ok(SegmentIter::new(
            self.persy_impl.scan(segment)?,
            self.persy_impl.clone(),
        ))
    }

    /// update the record content.
    ///
    ///
    /// This updated content can be read only with the [`read_record_tx`] till the transaction is committed.
    ///
    /// [`read_record_tx`]:struct.Persy.html#method.read_record_tx
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let new_data = vec![2;20];
    /// persy.update_record(&mut tx, "seg", &id, &new_data)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn update_record(&self, tx: &mut Transaction, segment: &str, id: &PersyId, rec: &[u8]) -> PRes<()> {
        self.persy_impl.update_record(&mut tx.0, segment, &id.0, rec)
    }

    /// delete a record.
    ///
    ///
    /// The record will result deleted only reading it whit [`read_record_tx`] till the transaction is committed.
    ///
    /// [`read_record_tx`]:struct.Persy.html#method.read_record_tx
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// persy.delete_record(&mut tx, "seg", &id)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn delete_record(&self, tx: &mut Transaction, segment: &str, id: &PersyId) -> PRes<()> {
        self.persy_impl.delete_record(&mut tx.0, segment, &id.0)
    }

    /// Create a new index with the name and the value management mode.
    ///
    /// The create operation require two template arguments that are the types as keys and
    /// values of the index this have to match the following operation on the indexes.
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::CLUSTER)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn create_index<K, V>(&self, tx: &mut Transaction, index_name: &str, value_mode: ValueMode) -> PRes<()>
    where
        K: IndexType,
        V: IndexType,
    {
        self.persy_impl.create_index::<K, V>(&mut tx.0, index_name, value_mode)
    }

    /// Drop an existing index.
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.drop_index(&mut tx, "my_new_index")?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn drop_index(&self, tx: &mut Transaction, index_name: &str) -> PRes<()> {
        self.persy_impl.drop_index(&mut tx.0, index_name)
    }

    /// Check if a segment already exist in the storage
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::REPLACE)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// assert!(persy.exists_index("my_new_index")?);
    /// # Ok(())
    /// # }
    /// ```
    ///
    pub fn exists_index(&self, segment: &str) -> PRes<bool> {
        self.persy_impl.exists_index(segment)
    }

    /// Check if a segment already exist in the storage considering the transaction
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::REPLACE)?;
    /// assert!(persy.exists_index_tx(&mut tx, "my_new_index")?);
    /// # let prepared = persy.prepare_commit(tx)?;
    /// # persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn exists_index_tx(&self, tx: &Transaction, segment: &str) -> PRes<bool> {
        self.persy_impl.exists_index_tx(&tx.0, segment)
    }

    /// Put a key value in an index following the value mode strategy.
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::CLUSTER)?;
    /// persy.put::<u8,u8>(&mut tx, "my_new_index",10,10)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn put<K, V>(&self, tx: &mut Transaction, index_name: &str, k: K, v: V) -> PRes<()>
    where
        K: IndexType,
        V: IndexType,
    {
        self.persy_impl.put::<K, V>(&mut tx.0, index_name, k, v)
    }

    /// Remove a key and optionally a specific value from an index following the value mode strategy.
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::CLUSTER)?;
    /// persy.put::<u8,u8>(&mut tx, "my_new_index",10,10)?;
    /// persy.remove::<u8,u8>(&mut tx, "my_new_index",10,Some(10))?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn remove<K, V>(&self, tx: &mut Transaction, index_name: &str, k: K, v: Option<V>) -> PRes<()>
    where
        K: IndexType,
        V: IndexType,
    {
        self.persy_impl.remove::<K, V>(&mut tx.0, index_name, k, v)
    }

    /// Get a value or a group of values from a key.
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode, Value};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// # let mut tx = persy.begin()?;
    /// # persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::CLUSTER)?;
    /// # persy.put::<u8,u8>(&mut tx, "my_new_index",10,10)?;
    /// # let prepared = persy.prepare_commit(tx)?;
    /// # persy.commit(prepared)?;
    /// let val = persy.get::<u8,u8>("my_new_index",&10)?;
    /// if let Some(is_there) = val {
    ///     // A value is actually there
    ///     match is_there {
    ///         Value::SINGLE(actual_value) => {
    ///         },
    ///         Value::CLUSTER(actual_value) => {
    ///         },
    ///     }
    /// }
    /// # Ok(())
    /// # }
    /// ```
    pub fn get<K, V>(&self, index_name: &str, k: &K) -> PRes<Option<Value<V>>>
    where
        K: IndexType,
        V: IndexType,
    {
        self.persy_impl.get::<K, V>(index_name, k)
    }

    /// Get a value or a group of values from a key considering changes in transaction.
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode, Value};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// # let mut tx = persy.begin()?;
    /// # persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::CLUSTER)?;
    /// persy.put::<u8,u8>(&mut tx, "my_new_index",10,10)?;
    /// let val = persy.get_tx::<u8,u8>(&mut tx,"my_new_index",&10)?;
    /// if let Some(is_there) = val {
    ///     // A value is actually there
    ///     match is_there {
    ///         Value::SINGLE(actual_value) => {
    ///         },
    ///         Value::CLUSTER(actual_value) => {
    ///         },
    ///     }
    /// }
    /// # let prepared = persy.prepare_commit(tx)?;
    /// # persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn get_tx<K, V>(&self, tx: &mut Transaction, index_name: &str, k: &K) -> PRes<Option<Value<V>>>
    where
        K: IndexType,
        V: IndexType,
    {
        self.persy_impl.get_tx::<K, V>(&mut tx.0, index_name, k)
    }

    /// Browse a range of keys and values from and index.
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode, Value,IndexIter};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// # let mut tx = persy.begin()?;
    /// # persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::CLUSTER)?;
    /// # persy.put::<u8,u8>(&mut tx, "my_new_index",10,10)?;
    /// # let prepared = persy.prepare_commit(tx)?;
    /// # persy.commit(prepared)?;
    /// let iter:IndexIter<u8,u8> = persy.range("my_new_index",10..12)?;
    /// for (k,val) in iter  {
    ///     // A value is actually there
    ///     match val {
    ///         Value::SINGLE(actual_value) => {
    ///         },
    ///         Value::CLUSTER(actual_value) => {
    ///         },
    ///     }
    /// }
    /// # Ok(())
    /// # }
    /// ```
    pub fn range<K, V, R>(&self, index_name: &str, range: R) -> PRes<IndexIter<K, V>>
    where
        K: IndexType,
        V: IndexType,
        R: RangeBounds<K>,
    {
        let (_, raw) = self.persy_impl.range(index_name, range)?;
        Ok(IndexIter::new(raw, self.persy_impl.clone()))
    }

    /// Browse a range of keys and values from and index including the transaction changes
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode, Value,TxIndexIter};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// let persy = Persy::open("./data.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::CLUSTER)?;
    /// persy.put::<u8,u8>(&mut tx, "my_new_index",10,10)?;
    /// {
    ///     let iter:TxIndexIter<u8,u8> = persy.range_tx(&mut tx,"my_new_index",10..12)?;
    ///     for (k,val) in iter  {
    ///         // A value is actually there
    ///         match val {
    ///             Value::SINGLE(actual_value) => {
    ///             },
    ///             Value::CLUSTER(actual_value) => {
    ///             },
    ///         }
    ///     }
    /// }
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn range_tx<'a, K, V, R>(
        &self,
        tx: &'a mut Transaction,
        index_name: &str,
        range: R,
    ) -> PRes<TxIndexIter<'a, K, V>>
    where
        K: IndexType,
        V: IndexType,
        R: RangeBounds<K>,
    {
        let (vm, raw) = self
            .persy_impl
            .range(index_name, (range.start_bound(), range.end_bound()))?;
        let tx_iter = tx.0.index_range::<K, V, R>(index_name, range);
        let tx_raw = TxIndexRawIter::new(index_name, tx_iter, Some(raw), vm);
        Ok(TxIndexIter::new(tx_raw, self.persy_impl.clone(), tx))
    }

    /// Rollback a not yet prepared transaction.
    ///
    /// All the resources used for eventual insert or update are released.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// persy.insert_record(&mut tx, "seg", &data)?;
    /// persy.rollback(tx)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn rollback(&self, tx: Transaction) -> PRes<()> {
        self.persy_impl.rollback(tx.0)
    }

    /// Prepare to commit a transaction, it will lock all the records involved in the transaction
    /// till a [`commit`] or [`rollback_prepared`] is called.
    ///
    /// [`commit`]:struct.Persy.html#method.commit
    /// [`rollback_prepared`]:struct.Persy.html#method.rollback_prepared
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// //Do what ever operations on the records
    /// let data = vec![1;20];
    /// persy.insert_record(&mut tx, "seg", &data)?;
    /// let _= persy.prepare_commit(tx)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn prepare_commit(&self, tx: Transaction) -> PRes<TransactionFinalize> {
        Ok(TransactionFinalize {
            persy_impl: self.persy_impl.clone(),
            finalize: self.persy_impl.prepare_commit(tx.0)?,
        })
    }

    /// Rollback a prepared commit.
    ///
    /// All the modification are rolled back and all the used resources are put released
    ///
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// //Do what ever operations on the records
    /// let data = vec![1;20];
    /// persy.insert_record(&mut tx, "seg", &data)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.rollback_prepared(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    ///
    pub fn rollback_prepared(&self, mut finalizer: TransactionFinalize) -> PRes<()> {
        self.persy_impl.rollback_prepared(&mut finalizer.finalize)?;
        Ok(())
    }

    /// Finalize the commit result of a prepared commit.
    ///
    /// All the operation done on the transaction are finalized all the lock released, all the
    /// old resources are released for reuse.
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared);
    /// # Ok(())
    /// # }
    /// ```
    ///
    pub fn commit(&self, mut finalizer: TransactionFinalize) -> PRes<()> {
        self.persy_impl.commit(&mut finalizer.finalize)?;
        Ok(())
    }

    /// List all the existing segments.
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_segment(&mut tx, "seg")?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// let segments = persy.list_segments()?;
    /// let names = segments.into_iter().map(|(name,_id)|name).collect::<Vec<String>>();
    /// assert!(names.contains(&"seg".to_string()));
    /// # Ok(())
    /// # }
    /// ```
    pub fn list_segments(&self) -> PRes<Vec<(String, SegmentId)>> {
        Ok(self
            .persy_impl
            .list_segments()?
            .into_iter()
            .map(|(name, id)| (name, SegmentId(id)))
            .collect())
    }

    /// List all the existing segments.
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_segment(&mut tx, "seg")?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// let segments = persy.list_segments()?;
    /// let names = segments.into_iter().map(|(name,_id)|name).collect::<Vec<String>>();
    /// assert!(names.contains(&"seg".to_string()));
    /// # Ok(())
    /// # }
    /// ```
    pub fn list_indexes(&self) -> PRes<Vec<(String, IndexInfo)>> {
        Ok(self.persy_impl.list_indexes()?)
    }

    /// List all the existing segments, considering all the changes in transaction.
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_segment(&mut tx, "seg")?;
    /// let segments = persy.list_segments_tx(&mut tx)?;
    /// let names = segments.into_iter().map(|(name,_id)|name).collect::<Vec<String>>();
    /// assert!(names.contains(&"seg".to_string()));
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn list_segments_tx(&self, tx: &mut Transaction) -> PRes<Vec<(String, SegmentId)>> {
        Ok(self
            .persy_impl
            .list_segments_tx(&mut tx.0)?
            .into_iter()
            .map(|(name, id)| (name, SegmentId(id)))
            .collect())
    }

    /// List all the existing indexes, considering changes in the transaction.
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_index::<u8, u8>(&mut tx, "idx", ValueMode::REPLACE)?;
    /// let indexes = persy.list_indexes_tx(&mut tx)?;
    /// let names = indexes.into_iter().map(|(name,_id)|name).collect::<Vec<String>>();
    /// assert!(names.contains(&"idx".to_string()));
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn list_indexes_tx(&self, tx: &mut Transaction) -> PRes<Vec<(String, IndexInfo)>> {
        Ok(self.persy_impl.list_indexes_tx(&mut tx.0)?)
    }
}
