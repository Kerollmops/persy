use crate::{
    persy::PersyImpl,
    segment::SegmentPageIterator,
    transaction::{Transaction, TransactionInsertIterator},
    PersyId, SegmentIter, TxSegmentIter,
};
use std::marker::PhantomData;

pub struct SegmentRawIter {
    segment_id: u32,
    iterator: SegmentPageIterator,
}

impl SegmentRawIter {
    pub fn new(segment_id: u32, iterator: SegmentPageIterator) -> SegmentRawIter {
        SegmentRawIter { segment_id, iterator }
    }
    pub fn next(&mut self, persy_impl: &PersyImpl) -> Option<(PersyId, Vec<u8>)> {
        while let Some(id) = self.iterator.next(persy_impl.address()) {
            if let Ok(Some(val)) = persy_impl.read_record_scan(self.segment_id, &id) {
                return Some((PersyId(id), val));
            }
        }
        None
    }
}

pub struct TxSegmentRawIter {
    segment_id: u32,
    tx_iterator: TransactionInsertIterator,
    iterator: SegmentPageIterator,
}

impl TxSegmentRawIter {
    pub fn new(tx: &mut Transaction, segment_id: u32, iterator: SegmentPageIterator) -> TxSegmentRawIter {
        let iter = tx.scan_insert(segment_id).into_iter();
        TxSegmentRawIter {
            segment_id,
            tx_iterator: iter,
            iterator,
        }
    }
    pub fn next(&mut self, persy_impl: &PersyImpl, tx: &mut Transaction) -> Option<(PersyId, Vec<u8>)> {
        while let Some(id) = self
            .iterator
            .next(persy_impl.address())
            .or_else(|| self.tx_iterator.next())
        {
            if let Ok(Some(val)) = persy_impl.read_record_scan_tx(tx, self.segment_id, &id) {
                return Some((PersyId(id), val));
            }
        }
        None
    }
}

/// IntoIterator of persistent records of a segment
#[deprecated(since = "0.5.0", note = "please use `scan/scan_tx` methods instead")]
pub struct RecordScanner<'a> {
    iterator: SegmentIter,
    ph: PhantomData<&'a PersyImpl>,
}

#[deprecated(since = "0.5.0", note = "please use `scan/scan_tx` methods instead")]
pub struct RecordIterator<'a> {
    iterator: SegmentIter,
    ph: PhantomData<&'a PersyImpl>,
}

/// Snapshot of a record from the segment iterator
#[deprecated(since = "0.5.0", note = "please use `scan/scan_tx` methods instead")]
pub struct IteratorItem {
    /// Identifier of the record can be used for do update or delete.
    ///
    pub id: PersyId,
    /// Content of the record
    ///
    pub content: Vec<u8>,
}

#[allow(deprecated)]
impl<'a> RecordScanner<'a> {
    pub fn new<'b>(iterator: SegmentIter) -> RecordScanner<'b> {
        RecordScanner::<'b> {
            iterator,
            ph: PhantomData,
        }
    }
}

#[allow(deprecated)]
impl<'a> IntoIterator for RecordScanner<'a> {
    type Item = IteratorItem;
    type IntoIter = RecordIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        RecordIterator::<'a> {
            iterator: self.iterator,
            ph: PhantomData,
        }
    }
}

#[allow(deprecated)]
impl<'a> Iterator for RecordIterator<'a> {
    type Item = IteratorItem;
    fn next(&mut self) -> Option<Self::Item> {
        self.iterator.next().map(|(id, content)| IteratorItem { id, content })
    }
}

/// IntoIterator of persistent records of a segment that include eventual in transaction changes
#[deprecated(since = "0.5.0", note = "please use `scan/scan_tx` methods instead")]
pub struct RecordScannerTx<'a> {
    iterator: TxSegmentIter<'a>,
}

#[deprecated(since = "0.5.0", note = "please use `scan/scan_tx` methods instead")]
pub struct RecordIteratorTx<'a> {
    iterator: TxSegmentIter<'a>,
}

#[allow(deprecated)]
impl<'a> RecordScannerTx<'a> {
    pub fn new(iterator: TxSegmentIter<'a>) -> RecordScannerTx<'a> {
        RecordScannerTx::<'a> { iterator }
    }
}

#[allow(deprecated)]
impl<'a> IntoIterator for RecordScannerTx<'a> {
    type Item = IteratorItem;
    type IntoIter = RecordIteratorTx<'a>;

    fn into_iter(self) -> Self::IntoIter {
        RecordIteratorTx::<'a> {
            iterator: self.iterator,
        }
    }
}

#[allow(deprecated)]
impl<'a> Iterator for RecordIteratorTx<'a> {
    type Item = IteratorItem;
    fn next(&mut self) -> Option<Self::Item> {
        self.iterator.next().map(|(id, content)| IteratorItem { id, content })
    }
}
